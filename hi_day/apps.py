from django.apps import AppConfig


class HiDayConfig(AppConfig):
    name = 'hi_day'
